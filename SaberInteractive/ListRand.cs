﻿using SaberInteractive.Node;

namespace SaberInteractive
{
    public class ListRand
    {
        public ListNode? Head;
        public ListNode? Tail;
        public int Count;

        public ListRand()
        {
            Head = null;
            Tail = null;
            Count = 0;
        }

        /// <summary>
        /// Добавление элементов в двусвязнный список
        /// </summary>
        /// <param name="data"></param>
        /// <param name="rand"></param>
        /// <returns></returns>
        public ListNode? Add(string data, bool rand = false)
        {
            if(string.IsNullOrEmpty(data)) { return null; }

            ListNode? node = new()
            {
                Data = data!.ToString()
            };

            CreateNode(node);
            SetRandomNode(rand, node);
            Count++;
            return node;
        }

        /// <summary>
        /// Чтение данных из файла
        /// </summary>
        /// <param name="s"></param>
        public void Deserialize(FileStream s)
        {
            if(s == null) { return; }

            ListNode? temp = new();
            Count = 0;

            List<string>? randomList = new();
            string? nodeString;

            List<ListNode>? listNodes = new();
            using (StreamReader? sr = new(s))
            {
                while ((nodeString = sr!.ReadLine()) != null)
                {
                    Count++;
                    randomList.Add(nodeString);
                    nodeString = sr!.ReadLine();
                    temp!.Data = nodeString;
                    ListNode? next = new();
                    temp.Next = next;
                    listNodes.Add(temp);
                    next!.Prev = temp;
                    temp = next;
                }
                Head = listNodes[0];
                Tail = listNodes[^1];
                Tail.Next = null;
            }

            RecoveryRandomNodeFromFile(listNodes, randomList);
        }

        /// <summary>
        /// Запись данных в файл
        /// </summary>
        /// <param name="s"></param>
        public void Serialize(FileStream s)
        {
            if( s == null) { return; }

            List<ListNode>? listNodes = CreateListNodes();

            using StreamWriter? sw = new(s);
            foreach (ListNode? node in listNodes)
            {   
                sw.WriteLine(listNodes.IndexOf(node!.Rand).ToString());
                sw.WriteLine(node.Data);
            }
        }

        /// <summary>
        /// Показ данных из списка
        /// </summary>
        public void Show()
        {
            if (Count == 0) { Console.WriteLine("List is empty"); return; }

            for (ListNode? node = Head; node != null; node = node!.Next)
            {
                Console.WriteLine($"data: {node.Data}, rand: {node?.Rand?.Data}");
            }

        }

        /// <summary>
        /// Получение элементов для сериализации
        /// </summary>
        /// <returns></returns>
        private List<ListNode> CreateListNodes()
        {
            List<ListNode>? listNodes = new()
            {
                Head
            };

            for (int i = 0; i < Count - 1; i++)
            {
                listNodes.Add(listNodes[i].Next);
            }

            return listNodes;
        }

        /// <summary>
        /// Заполнение элемента для списка
        /// </summary>
        /// <param name="node"></param>
        private void CreateNode(ListNode node)
        {
            if (Head == null) { Head = Tail = node; return; }
            node.Prev = Tail;
            Tail!.Next = node;
            Tail = node;
        }

        /// <summary>
        /// Получение рандомного элемента
        /// </summary>
        /// <param name="rand"></param>
        /// <param name="node"></param>
        private void SetRandomNode(bool rand, ListNode node)
        {
            if (!rand) { node.Rand = null; return; }
            Random random = new();
            int index = random.Next(0, Count);
            CheakRandomNode(node, index);
        }

        /// <summary>
        /// Проверка возможности установки
        /// </summary>
        /// <param name="node"></param>
        /// <param name="index"></param>
        private void CheakRandomNode(ListNode node, int index)
        {
            if (index <= Count) { CheakTheIndexForRandomPlaceNode(node, index); return; }
        }

        /// <summary>
        /// Проверка индекса для получения рандомного уэлемента в списке
        /// </summary>
        /// <param name="node"></param>
        /// <param name="index"></param>
        private void CheakTheIndexForRandomPlaceNode(ListNode node, int index)
        {
            ListNode? randomNode = index <= Count / 2 ? SetRandomHeadNode(index) : SetRandomTailNode(index);
            node.Rand = randomNode;
        }

        /// <summary>
        /// Восстановление структуры списка
        /// </summary>
        /// <param name="listNodes"></param>
        /// <param name="randomList"></param>
        private void RecoveryRandomNodeFromFile(List<ListNode> listNodes, List<string> randomList)
        {
            for (int i = 0; i < Count; i++)
            {
                try
                {
                    int value = int.Parse(randomList[i]);
                    if (value != -1) { listNodes[i].Rand = listNodes[value]; }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error! on the element {i + 1}", e);
                }
            }
        }

        /// <summary>
        /// Установка рандомного элемента в конец списка
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private ListNode? SetRandomTailNode(int index)
        {
            ListNode? randomNode = Tail;
            for (int i = Count; i != index; --i)
            {
                randomNode = randomNode?.Prev;
                return randomNode;
            }

            return randomNode;
        }

        /// <summary>
        /// Установка рандомного элемента в начало списка
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private ListNode? SetRandomHeadNode(int index)
        {
            ListNode? randomNode = Head;
            for (int j = 1; j != index; ++j)
            {
                randomNode = randomNode?.Next;
                return randomNode;
            }
            return randomNode;
        }
    }
}
