﻿namespace SaberInteractive
{
    partial class Program
    {
        static void Main(string[] args)
        {
            ListRand inputList = new();
            ListRand outputList = new();

            inputList.Add("AAA AA A", true);
            inputList.Add("BBB BB BB");
            inputList.Add("CCCC", true);
            inputList.Add("DD");
            inputList.Add("EEEEE", true);
            inputList.Add("FFFF");

            inputList.Show();

            const string path = "input.txt";
            using (FileStream? file = new(path, FileMode.Create))
            {
                inputList.Serialize(file);
            }

            using(FileStream file = File.OpenRead(path))
            {
                outputList.Deserialize(file);
                outputList.Show();
            }
        }
    }
}